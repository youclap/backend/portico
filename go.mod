module gitlab.com/youclap/backend/portico

go 1.12

require (
	firebase.google.com/go v3.8.1+incompatible
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	golang.org/x/net v0.0.0-20190628185345-da137c7871d7
	google.golang.org/api v0.7.0
)
